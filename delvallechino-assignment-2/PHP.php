<?php  
  $age = 21;


  echo "<br>If-Elseif-Else<br>";
  if($age > 21) {
    echo "You're over 21 years old!<br>";
  }elseif($age == 21) {
    echo "You're 21 years old!<br>";
  }else {
    echo "You're below 21 years old!<br>";
  }

  echo "<br>While Loop<br>";
  $i = 0;
  while($i < 5) {
    echo "Onii-chan!<br>";
    $i++;
  }

  echo "<br>Do-While Loop<br>";
  $i = 0;
  do {
    echo "Onii-chan!<br>";
    $i++;
  }while($i < 5);

  echo "<br>For Loop<br>";
  for($i = 0; $i < 5; $i++) {
    echo "Onii-chan!<br>";
  }

  echo "<br>ForEach Loop<br>";
  $num = array(20, 40, 54, 120, 53);
  foreach($num as $n) {
    echo $n . "<br>";
  }
?>