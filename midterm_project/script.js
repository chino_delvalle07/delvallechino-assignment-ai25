var app = new Vue({
    el: '#app',
    data: {
        phonebookinfo: '',
        name: null,
        phonenumber: null
    },
    mounted: function () {
        axios.post('request.php',
        {
            request: 'getdata',
        })
        .then(res => {
            app.phonebookinfo = res.data;
        })
        .catch(err => {
            console.error(err); 
        })
    },
    methods: {
        insertInfo : function() {
            axios.post('request.php',
            {
                request: 'insert',
                name: this.name,
                phonenumber: this.phonenumber
            })
            .then(res => {
                alert(res.data);
                window.location.reload();
            })
            .catch(err => {
                console.log(err);
            })
        },
        deleteInfo : function(index) {
                axios.post('request.php',
                {
                    request: 'delete',
                    id: index
                })
                .then(res => {
                    alert(res.data);
                    window.location.reload();
                })
                .catch(err => {
                    console.log(err);
                })
        },
    }
});